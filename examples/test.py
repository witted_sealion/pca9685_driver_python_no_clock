# Simple demo of of the PCA9685 PWM servo/LED controller library.
# This will move channel 0 from min to max position repeatedly.
# Author: Tony DiCola
# License: Public Domain
from __future__ import division
import time

# Import the PCA9685 module.
import Adafruit_PCA9685


pwm = Adafruit_PCA9685.PCA9685(address=0x40, busnum=1)

pwm_frequency = 400
prescaler = round(25000000.0 / (4096 * pwm_frequency)) - 1 

#pwm.set_prescaler(int(prescaler))
pwm.set_pwm_freq(pwm_frequency)

print('Moving servo on channel 0, press Ctrl-C to quit...')
while True:
    value = input(">>>")
    pwm.write_microseconds(0, int(value))
    pwm.write_microseconds(1, int(value))
    pwm.write_microseconds(2, int(value))
    pwm.write_microseconds(3, int(value))
